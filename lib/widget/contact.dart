import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nearest_aider/models/contact_model.dart';
import 'package:nearest_aider/pages/get_location_map.dart';
import 'package:nearest_aider/provider.dart';
import 'package:nearest_aider/utilis/connection_manger.dart';
import 'package:nearest_aider/utilis/response.dart';
import 'package:nearest_aider/widget/button.dart';
import 'package:nearest_aider/widget/text_field.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Contact extends StatelessWidget {
  ContactModel? contactModel;

  LatLng? address;

  late final TextEditingController userName;
  late final TextEditingController mobile;

  Contact({this.contactModel}){
    userName=TextEditingController(text: contactModel!.name);
    mobile=TextEditingController();
  }

  String? validateUsername(String username){
    if (username.isEmpty) {
      return "please enter username";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Location",style: TextStyle(fontFamily: "Lato",fontSize: 12,color: Colors.grey),textAlign: TextAlign.left,),
          SizedBox(
            height: height*0.009,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(contactModel!.name!,style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 18),textAlign: TextAlign.left),
              Row(
                children: [
                  InkWell(
                      onTap: (){
                        showDialog(context: context, builder: (BuildContext context){
                          return updateContactDialog(context, height, width);
                        });
                      },
                      child: Text("Update",style: TextStyle(decoration: TextDecoration.underline,fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(167, 22, 10, 1)),)),
                  SizedBox(width: width*0.03),
                  InkWell(
                      onTap: (){
                        showDialog(context: context, builder: (BuildContext context){
                          return deleteContactDialog(context, height, width);
                        });
                      },
                      child: Text("Delete",style: TextStyle(decoration: TextDecoration.underline,fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(167, 22, 10, 1)),)),
                ],
              )
            ],
          ),
          SizedBox(
            height: height*0.009,
          ),
          Divider(
            thickness: 1,
          )
        ],
      ),
    );
  }

  Widget deleteContactDialog(BuildContext context,double height,double width){
    return AlertDialog(
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide.none
      ),
      title: Text("Delete",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 16,color: Color.fromRGBO(248, 19, 0, 1)),textAlign: TextAlign.center,),
      content: Text("Are you sure to delete the contact?",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(28, 28, 28, 1)),),
      actions: [
        Row(
          children: [
            ButtonFormWidget(minWidth: width*0.6, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Delete", textColor: Colors.white, textSize: 15,
              onPressed: ()async{
                Response response=await deleteContact(contactModel!.contactId!);
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(response.body,)));
                  Navigator.of(context).pop();

              },),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        Row(
          children: [
            ButtonFormWidget(minWidth: width*0.6, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Cancel", textColor: Colors.white, textSize: 15,onPressed: (){Navigator.of(context).pop();},),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ],
    );
  }

  Widget updateContactDialog(BuildContext context,double height,double width){
    return AlertDialog(
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide.none
      ),
      title: Text("Update contact",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 16,color: Color.fromRGBO(248, 19, 0, 1)),textAlign: TextAlign.center,),
      content: SingleChildScrollView(
        child: Container(
          height: height*0.22,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(left: width*0.05,right: width*0.05,bottom: height*0.03),
                child: TextInput(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 5, controller: userName, textInputType: TextInputType.emailAddress, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "User Name", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red, validate:(value) {return validateUsername(value!);},),
              ),
              InkWell(
                  onTap: () async {var result =await Navigator.of(context).push(MaterialPageRoute(builder: (builder)=>GetLocationMap())); address=result; Provider.of<AppProvider>(context,listen: false).changeAddressLabel(address!.latitude.toString()+" "+address!.longitude.toString()); },
                  child: Padding(
                    padding: EdgeInsets.only(left: width*0.06,right: width*0.06),
                    child: Container(
                      //margin: EdgeInsets.only(top: height*0.02,bottom: height*0.02),
                      padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                      height: height*0.071,
                      width: width*0.5,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border: Border.all(width: 0.5)
                      ),
                      child: Row(

                        children: [
                          Flexible(child: Text(Provider.of<AppProvider>(context).addressLabel,style: TextStyle(color: Colors.grey,fontSize: 12,),overflow: TextOverflow.ellipsis)),
                        ],
                      ),
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
      actions: [
        Row(
          children: [
            ButtonFormWidget(minWidth: width*0.6, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Update", textColor: Colors.white, textSize: 15,
              onPressed: () async{
                if(!userName.text.isEmpty&&address!=null){
                  Response response=await updateContact(userName.text.toString(), address!.latitude, address!.longitude, contactModel!.contactId!);
                  if(response.statusCode==200){
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(response.body,)));
                    Navigator.of(context).pop();
                  }
                }
              },),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        Row(
          children: [
            ButtonFormWidget(minWidth: width*0.6, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Cancel", textColor: Colors.white, textSize: 15,onPressed: (){Navigator.of(context).pop();},),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ],
    );
  }
}
