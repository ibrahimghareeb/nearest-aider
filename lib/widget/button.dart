
import 'package:flutter/material.dart';



class ButtonFormWidget extends StatelessWidget {
  late final GestureTapCallback  onPressed;
  late double minWidth;
  late double borderRadius;
  late Color borderColor;
  late double borderWidth;
  late String buttonText;
  late Color textColor;
  late double textSize;


  ButtonFormWidget({required this.minWidth,required this.borderRadius,required this.borderColor,required this.borderWidth,required this.buttonText,required this.textColor,required this.textSize,required this.onPressed});


  @override
  Widget build(BuildContext context) {
    return  MaterialButton(
      minWidth: minWidth,

      child:  Text(buttonText,style: TextStyle(
          color: textColor,
          fontSize: textSize
      ),),
      color: Color.fromRGBO(167, 22, 10, 1),
      shape:  OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
          borderSide: BorderSide(
              width: borderWidth,
              color: borderColor
          )
      ),
      onPressed: onPressed,


    );
  }
}
