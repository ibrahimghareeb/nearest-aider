import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class TextInput extends StatelessWidget {
  late double leftMargin;
  late double topMargin;
  late double rightMargin;
  late double bottomMargin;
  late final TextEditingController controller;
  late TextInputType textInputType;
  late double fontSizeAll;
  late Color colorFontStyle;
  late String hintText;
  late double hintSize;
  late Color hintColor;
  late double labelSize;
  late Color labelColor;
  late double errorSize;
  late Color errorColor;
  late double borderRadius;
  late double borderWidth;
  late Color borderColor;
  late double enabledBorderRadius;
  late double enabledBorderWidth;
  late Color enabledBorderColor;
  late double focusedBorderRadius;
  late double focusedBorderWidth;
  late Color focusedBorderColor;
  late double errorBorderRadius;
  late double errorBorderWidth;
  late Color errorBorderColor;
  late Icon? suffixIcon;
  late Color? backgroundColor=Colors.white;
  late final String? Function(String?)? validate;
  late int? maxLine=1;
  late final GestureTapCallback? onEditingComplete;
  TextInput({required this.leftMargin,required this.topMargin,required this.rightMargin,required this.bottomMargin,required this.controller,required this.textInputType,required this.fontSizeAll,required this.colorFontStyle,required this.hintText,required this.hintSize,required this.hintColor,required this.labelSize,required this.labelColor,required this.errorSize,required this.errorColor,required this.borderRadius,required this.borderWidth,required this.borderColor,required this.enabledBorderRadius,required this.enabledBorderWidth,required this.enabledBorderColor,required this.focusedBorderRadius,required this.focusedBorderWidth,required this.focusedBorderColor,required this.errorBorderRadius,required this.errorBorderWidth,required this.errorBorderColor,required this.validate,this.suffixIcon,this.maxLine,this.backgroundColor,this.onEditingComplete,this.isEnabled});
  bool? isEnabled;
  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.fromLTRB(leftMargin, topMargin, rightMargin, bottomMargin),
      child: TextFormField(
        enabled: isEnabled!=null? isEnabled:true,
        controller: controller,
        keyboardType: textInputType,
        style: TextStyle(
          fontSize: fontSizeAll,
          color: colorFontStyle,
        ),
        maxLines: maxLine,
        onEditingComplete: onEditingComplete!=null?onEditingComplete:(){},
        decoration: InputDecoration(
          filled: true,
          fillColor: backgroundColor!=null?backgroundColor:Colors.white,
          contentPadding: EdgeInsets.only(left: width*0.06),
          hintText: hintText,
          //suffixIcon: suffixIcon!=null?suffixIcon:null,
          //prefixIcon: Icon(null),
          hintStyle: TextStyle(
            fontSize: hintSize,
            color: hintColor,
          ),
          labelStyle: TextStyle(
            fontSize: labelSize,
            color: labelColor,
          ),
          errorStyle: TextStyle(
              fontSize: errorSize,
              color: errorColor,
            overflow: TextOverflow.clip
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
              borderSide: BorderSide(
                width: borderWidth,
                color: borderColor,
              )
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(enabledBorderRadius)),
              borderSide: BorderSide(
                width: enabledBorderWidth,
                color: enabledBorderColor,
              )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(focusedBorderRadius)),
              borderSide: BorderSide(
                width: focusedBorderWidth,
                color: focusedBorderColor,
              )
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(errorBorderRadius)),
              borderSide: BorderSide(
                width: errorBorderWidth,
                color: errorBorderColor,
              )
          ),

        ),
        validator: validate,


      ),
    );
  }
}


class TextInputPassword extends StatefulWidget {
  late double leftMargin;
  late double topMargin;
  late double rightMargin;
  late double bottomMargin;
  late final TextEditingController controller;
  late TextInputType textInputType;
  late double fontSizeAll;
  late Color colorFontStyle;
  late String hintText;
  late double hintSize;
  late Color hintColor;
  late double labelSize;
  late Color labelColor;
  late double errorSize;
  late Color errorColor;
  late double borderRadius;
  late double borderWidth;
  late Color borderColor;
  late Icon suffixIcon;
  late double enabledBorderRadius;
  late double enabledBorderWidth;
  late Color enabledBorderColor;
  late double focusedBorderRadius;
  late double focusedBorderWidth;
  late Color focusedBorderColor;
  late double errorBorderRadius;
  late double errorBorderWidth;
  late Color errorBorderColor;
  late bool resgister;
  late final String? Function(String?)? validate;
  TextInputPassword({required this.leftMargin,required this.topMargin,required this.rightMargin,required this.bottomMargin,required this.controller,required this.textInputType,required this.fontSizeAll,required this.colorFontStyle,required this.hintText,required this.hintSize,required this.hintColor,required this.labelSize,required this.labelColor,required this.errorSize,required this.errorColor,required this.borderRadius,required this.borderWidth,required this.borderColor,required this.enabledBorderRadius,required this.enabledBorderWidth,required this.enabledBorderColor,required this.focusedBorderRadius,required this.focusedBorderWidth,required this.focusedBorderColor,required this.errorBorderRadius,required this.errorBorderWidth,required this.errorBorderColor,required this.suffixIcon,required this.validate,required this.resgister});

  @override
  State<TextInputPassword> createState() => _TextInputPasswordState();
}

class _TextInputPasswordState extends State<TextInputPassword> {
  late bool obscure=true;
  late bool ceckboxVal=false;
  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.fromLTRB(widget.leftMargin, widget.topMargin, widget.rightMargin, widget.bottomMargin),
      child: TextFormField(
        obscureText: obscure,
        controller: widget.controller,
        keyboardType: widget.textInputType,
        style: TextStyle(
          fontSize: widget.fontSizeAll,
          color: widget.colorFontStyle,
        ),
        decoration: InputDecoration(
          suffixIcon: IconButton(icon: Icon(obscure?Icons.remove_red_eye:Icons.visibility_off,size: 20,color: Color(0xFF303030),),onPressed: (){
            setState(() {
              obscure=!obscure;
            });
          },),
          contentPadding: EdgeInsets.only(left: width*0.06),
          hintText: widget.hintText,
          hintStyle: TextStyle(
            fontSize: widget.hintSize,
            color: widget.hintColor,
          ),
          labelStyle: TextStyle(
            fontSize: widget.labelSize,
            color: widget.labelColor,
          ),
          errorStyle: TextStyle(
              fontSize: widget.errorSize,
              color: widget.errorColor
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.borderRadius)),
              borderSide: BorderSide(
                width: widget.borderWidth,
                color: widget.borderColor,
              )
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.enabledBorderRadius)),
              borderSide: BorderSide(
                width: widget.enabledBorderWidth,
                color: widget.enabledBorderColor,
              )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.focusedBorderRadius)),
              borderSide: BorderSide(
                width: widget.focusedBorderWidth,
                color: widget.focusedBorderColor,
              )
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(widget.errorBorderRadius)),
              borderSide: BorderSide(
                width: widget.errorBorderWidth,
                color: widget.errorBorderColor,
              )
          ),
        ),
        validator: widget.validate,

      ),
    );
  }
}
