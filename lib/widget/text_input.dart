import 'package:flutter/material.dart';


class TextInputAPP extends StatelessWidget {
  TextEditingController? controller;
  String? hintText;
  String? Function(String?)? validate;


  TextInputAPP({this.controller, this.hintText, this.validate});

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
      child: TextFormField(
        controller: controller,
        validator: validate,
        decoration: InputDecoration(
          hintText: hintText,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(
              color: Colors.grey.withOpacity(0.5),
            )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              )
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(
                color: Colors.red,
              )
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              )
          ),
        ),
      ),
    );
  }
}
