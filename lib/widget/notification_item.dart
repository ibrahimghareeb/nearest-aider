import 'package:flutter/material.dart';

class NotificationItem extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //Text("13/9/2022",style: TextStyle(fontFamily: "Lato",fontSize: 12,color: Colors.grey),textAlign: TextAlign.left,),
          SizedBox(
            height: height*0.009,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("injured ",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 18),textAlign: TextAlign.left),
              Container(
                width: width*0.055,
                height: height*0.03,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  color: Color.fromRGBO(248, 19, 0, 1)
                ),
              )
            ],
          ),
          SizedBox(
            height: height*0.009,
          ),
          Divider(
            thickness: 1,
          )
        ],
      ),
    );
  }
}
