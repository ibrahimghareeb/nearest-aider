import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppProvider with ChangeNotifier{
  String addressLabel="Location";
  int introCounter=0;
  String introText="Find your nearest aider";
  SharedPreferences? sharedPreferences;
  void changeAddressLabel(String val){
    addressLabel=val;
    notifyListeners();
  }
  void changeIntroText(){
    introCounter=(++introCounter)%3;
    if (introCounter==1) {
      introText="Hayat App is your near savior";
    } else if (introCounter==2) {
      introText="Save your life";
    }
    notifyListeners();
  }
}