import 'package:http/http.dart' as http;
import 'package:nearest_aider/models/contact_model.dart';
import 'package:nearest_aider/models/login_model.dart';
import 'package:nearest_aider/models/notification_model.dart';
import 'package:nearest_aider/utilis/response.dart';
import 'dart:convert';
import 'dart:io';
final String baseUrl="https://megq.000webhostapp.com";


Future<Response> login(String username,String password) async {
  var url=Uri.parse("$baseUrl/login.php");
  http.Response response=await http.post(url,headers:  {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: jsonEncode(<String,String>{
      "username":username,
      "password":password
  })
  );
  var data=json.decode(response.body);
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  return Response(statusCode: 404,body: "failed connect to server");
}

// Future <Response> login(String email, String password) async{
//   var url=Uri.parse("$baseUrl/login.php");
//   http.Response response = await http.post(url, headers: {
//     'content_type': 'application/json',
//     'accept': 'application/json'
//   },body: jsonEncode(<String, String>{
//     "username":email,
//     "password":password
//   }),);
//   var data=json.decode(response.body.toString());
//   if(response.statusCode==200){
//     LoginModel model=LoginModel.fromJson(data);
//     return Response(statusCode: 200,body: model);
//   }
//   else if (response.statusCode==401){
//     return Response(statusCode: 401,body: "login failed");
//   }
//   return Response();
// }

Future <Response> signup(String username, String password,String phone,double latitude,double longitude,String role) async{
  var url=Uri.parse("$baseUrl/register2.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  }, body: {
    "phone":phone,
    "username": username,
    "password": password,
    "latitude":"0",
    "longitude":"0",
    "role":role,
  });
  print(response.body);
  var data=json.decode(response.body);
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  else if (response.statusCode==401){
    return Response(statusCode: 401,body: "register failed");
  }
  return Response();
}

Future <Response> paramedicSignup(String username, String password,String phone,double latitude,double longitude,String role,File image) async{
  var url=Uri.parse("$baseUrl/register2.php");
  http.MultipartRequest request=http.MultipartRequest('POST',url);
  request.fields.addAll({
      "phone":phone,
      "username": username,
      "password": password,
      "latitude":latitude.toString(),
      "longitude":longitude.toString(),
      "role":role,
  });
  request.files.add(http.MultipartFile("myImage",image.readAsBytes().asStream(),image.lengthSync(),filename: image.path.split("/").last));
  http.StreamedResponse response=await request.send();
  var data=json.decode(await response.stream.bytesToString());
  print(data.toString());
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }

  else
    return Response(statusCode: 400,body: "failed to connect");
}

Future <Response> editAccount(String email, String password,String phone,double latitude,double longitude,String id) async{
  var url=Uri.parse("$baseUrl/editUser.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: {
    "username":email,
    "phone":phone,
    "password":password,
    "latitude":latitude.toString(),
    "longitude":longitude.toString(),
    "id":id
  });
  var data=json.decode(response.body);

  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  else if (response.statusCode==401){
    return Response(statusCode: 401,body: "login failed");
  }
  return Response();
}

Future <Response> addContact(String email, double latitude,double longitude,String id) async{
  var url=Uri.parse("$baseUrl/addContact.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: {
    "name":email,
    "latitude":latitude.toString(),
    "longitude":longitude.toString(),
    "user_id":id
  });
  var data=json.decode(response.body);
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  else if (response.statusCode==404){
    return Response(statusCode: 401,body: "login failed");
  }
  return Response();
}

Future<Response> getMyContact(String id) async{
  var url=Uri.parse("$baseUrl/readContactById.php?id="+id);
  http.Response response = await http.get(url, headers: {
  'content_type': 'application/json',
  'accept': 'application/json'
  },);
  if(response.statusCode==200){
    var data=json.decode(response.body);
    List<ContactModel> contactList=(data as List).map((e) => ContactModel.fromJson(e)).toList();
    return Response(statusCode: 200,body: contactList);
  }
  else
    return Response(statusCode: 404,body: "you don't have any contact");

}

Future <Response> updateContact(String email, double latitude,double longitude,String id) async{
  var url=Uri.parse("$baseUrl/editContact.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: {
    "name":email,
    "latitude":latitude.toString(),
    "longitude":longitude.toString(),
    "id":id
  });
  var data=json.decode(response.body);
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model.message);
  }
  else if (response.statusCode==404){
    return Response(statusCode: 401,body: "login failed");
  }
  return Response();
}

Future <Response> deleteContact(String id) async{
  var url=Uri.parse("$baseUrl/deleteContact.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: json.encode(id));
  if(response.statusCode==200){
    print(response.body);
    return Response(statusCode: 200,body: response.body);
  }
  else if (response.statusCode==404){
    return Response(statusCode: 401,body: "login failed");
  }
  return Response();
}

Future<Response> addEmergencyRequest(String userId) async{
  var url=Uri.parse("$baseUrl/addRequest.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: jsonEncode(<String, String>{
    "type":"emergency",
    "description":"please help",
    "user_id":userId
  }));
  var data=json.decode(response.body);
  print(response.body);
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  return Response(statusCode: 404,body: "error in add request");
}



Future<Response> getNotification() async{
  var url=Uri.parse("$baseUrl/getNotifications.php");
  http.Response response = await http.get(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },);
  if(response.statusCode==200){
    var data=json.decode(response.body);
    List<NotificationModel> notificationList=(data as List).map((e) => NotificationModel.fromJson(e)).toList();
    return Response(statusCode: 200,body: notificationList);
  }
  else
    return Response(statusCode: 404,body: "you don't have any notification");
}