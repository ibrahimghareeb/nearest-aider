import 'package:flutter/material.dart';
import 'package:nearest_aider/pages/login_with_api.dart';
import 'package:nearest_aider/pages/map_page.dart';
import 'package:nearest_aider/pages/splash_screen.dart';
import 'package:nearest_aider/provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp( ChangeNotifierProvider(create: (_)=>AppProvider(),child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}
