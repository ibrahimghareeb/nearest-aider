import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nearest_aider/models/notification_model.dart';
import 'package:nearest_aider/pages/get_location_map.dart';
import 'package:nearest_aider/utilis/connection_manger.dart';
import 'package:nearest_aider/utilis/response.dart';
import 'package:nearest_aider/widget/notification_item.dart';

class MyNotifications extends StatefulWidget {
  const MyNotifications({Key? key}) : super(key: key);

  @override
  State<MyNotifications> createState() => _MyNotificationsState();
}

class _MyNotificationsState extends State<MyNotifications> {

  List<NotificationModel>? notificationList;
  bool isLoaded=false;

  Future<void> getNotificationFromAPI() async{
    Response response=await getNotification();
    if(response.statusCode==200){
      notificationList=response.body;
    }
    setState(() {
      isLoaded=true;
    });
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    getNotificationFromAPI();
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 251, 250, 1),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("My notifications",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 24,color: Color.fromRGBO(248, 19, 0, 1))),
      ),
      body: Container(
        width: width,
        height: height,
        child: Column(
          children: !isLoaded?[Container(width:width,child: Row(children: [CircularProgressIndicator(color: Color.fromRGBO(248, 19, 0, 1),)], mainAxisAlignment: MainAxisAlignment.center,))]:
          notificationList!=null?
          notificationList!.map((e) =>
              InkWell(onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (builder)=>GetLocationMap(address: LatLng(double.parse(e.latitude!),double.parse(e.longitude!)))));
          },child: NotificationItem())).toList():[Text("you don't have any contact")]
        ),
      ),
    );
  }
}
