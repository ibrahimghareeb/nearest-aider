import 'package:flutter/material.dart';
import 'package:nearest_aider/pages/home.dart';
import 'package:nearest_aider/pages/home_paramedic.dart';
import 'package:nearest_aider/pages/intro.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isFirst=true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void initialize(BuildContext context) async {
    SharedPreferences data= await SharedPreferences.getInstance();
    if(!data.containsKey("first time")){
      data.setBool("first time", true);
      Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> Intro()),ModalRoute.withName("/intro")));
    }else if(data.containsKey("login")&&data.getBool("login")!){
      if(data.getString("accountType")=="paramedic"){
        Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> HomePageParamedic(data.getString("username"))),ModalRoute.withName("/homeParamedic")));
      }else{
        Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> HomePage(data.getString("username"))),ModalRoute.withName("/home")));
      }
    }
    else{
      Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> Login()),ModalRoute.withName("/login")));
    }
  }


  @override
  Widget build(BuildContext context) {
    if(isFirst){
      initialize(context);
      isFirst=false;
    }
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return SafeArea(
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/images/Splash.png",)
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/images/logo.png")
          ],
        ),
      ),
    );
  }
}
