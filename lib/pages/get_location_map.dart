
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class GetLocationMap extends StatefulWidget {
  LatLng? address;
  GetLocationMap({this.address});

  @override
  _GetLocationMapState createState() => _GetLocationMapState();
}

class _GetLocationMapState extends State<GetLocationMap> {
  List<Marker> myMarker = [];
  LatLng? markerLocation;

  @override
  Widget build(BuildContext context) {
    if(widget.address!=null){
      myMarker.add(
        Marker(
          markerId: MarkerId(widget.address.toString()),
          position: widget.address!,
          icon:
          BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
        ),
      );
    }
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
        body: Stack(
          children: [
            GoogleMap(
              initialCameraPosition:  CameraPosition(
                target: widget.address!=null?widget.address!:LatLng(0, 0),
                zoom: widget.address!=null?14: 0,
              ),
              onTap: widget.address==null? _handleTap: (tapped){},
              myLocationButtonEnabled: true,
              markers: Set.from(myMarker),
              mapType: MapType.hybrid,
              zoomControlsEnabled: false,
              myLocationEnabled: true,
            ),
            Container(
              width: width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  widget.address!=null?Container():MaterialButton(
                    shape: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide:BorderSide.none
                    ),
                    minWidth: width*0.9,
                    onPressed: (){
                      Navigator.pop(context, markerLocation);
                    },
                    color: Color.fromRGBO(167, 22, 10, 1),
                    child: Text("Confirm",style: TextStyle(color: Colors.white),),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  _handleTap(LatLng tappedPoint) {
    markerLocation = tappedPoint;
    setState(() {
      myMarker = [];
      myMarker.add(
        Marker(
          markerId: MarkerId(tappedPoint.toString()),
          position: tappedPoint,
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
        ),
      );
    });
  }
}
