import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nearest_aider/models/contact_model.dart';
import 'package:nearest_aider/pages/get_location_map.dart';
import 'package:nearest_aider/provider.dart';
import 'package:nearest_aider/utilis/connection_manger.dart';
import 'package:nearest_aider/utilis/response.dart';
import 'package:nearest_aider/widget/button.dart';
import 'package:nearest_aider/widget/contact.dart';
import 'package:nearest_aider/widget/text_field.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyContact extends StatefulWidget {

  @override
  State<MyContact> createState() => _MyContactState();
}

class _MyContactState extends State<MyContact> {

  final TextEditingController userName=TextEditingController();
  LatLng? address;
  List<ContactModel>? contactList;
  bool isLoaded=false;
  String? validateUsername(String username){
    if (username.isEmpty) {
      return "please enter username";
    } else {
      return null;
    }
  }

  Future<void> getContactFromAPI() async{
    SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
    Response response=await getMyContact(sharedPreferences.getString("id")!);
    if(response.statusCode==200){
      contactList=response.body;
    }
    setState(() {
      isLoaded=true;
    });
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    getContactFromAPI();
  }


  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 251, 250, 1),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(248, 19, 0, 1),
        onPressed: (){
          showDialog(context: context, builder: (BuildContext context){
            return addContactDialog(context, height, width);
          });
        },
      ),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("My contact",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 24,color: Color.fromRGBO(248, 19, 0, 1))),
      ),
      body: Container(
        width: width,
        height: height,
        child: Column(
          mainAxisAlignment: isLoaded&&contactList!=null?MainAxisAlignment.start:MainAxisAlignment.center,
          children: !isLoaded?[Container(width:width,child: Row(children: [CircularProgressIndicator(color: Color.fromRGBO(248, 19, 0, 1),)],mainAxisAlignment: MainAxisAlignment.center,))]:
              contactList!=null?
          contactList!.map((e) => Contact(contactModel: e,)).toList():[Text("you don't have any contact")]
        ),
      ),
    );
  }

  Widget addContactDialog(BuildContext context,double height,double width){
    return AlertDialog(
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide.none
      ),
      title: Text("Add new contact",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 16,color: Color.fromRGBO(248, 19, 0, 1)),textAlign: TextAlign.center,),
      content: SingleChildScrollView(
        child: Container(
          height: height*0.22,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(left: width*0.05,right: width*0.05,bottom: height*0.03),
                child: TextInput(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 5, controller: userName, textInputType: TextInputType.emailAddress, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "User Name", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red, validate:(value) {return validateUsername(value!);},),
              ),
              InkWell(
                  onTap: () async {var result =await Navigator.of(context).push(MaterialPageRoute(builder: (builder)=>GetLocationMap())); address=result; Provider.of<AppProvider>(context,listen: false).changeAddressLabel(address!.latitude.toString()+" "+address!.longitude.toString()); },
                  child: Padding(
                    padding: EdgeInsets.only(left: width*0.06,right: width*0.06),
                    child: Container(
                      //margin: EdgeInsets.only(top: height*0.02,bottom: height*0.02),
                      padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                      height: height*0.071,
                      width: width*0.5,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border: Border.all(width: 0.5)
                      ),
                      child: Row(

                        children: [
                          Flexible(child: Text(Provider.of<AppProvider>(context).addressLabel,style: TextStyle(color: Colors.grey,fontSize: 12,),overflow: TextOverflow.ellipsis)),
                        ],
                      ),
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
      actions: [
        Row(
          children: [
            ButtonFormWidget(minWidth: width*0.6, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Add", textColor: Colors.white, textSize: 15,
              onPressed: () async{
                if(!userName.text.isEmpty&&address!=null){
                  SharedPreferences temp=await SharedPreferences.getInstance();
                  Response response=await addContact(userName.text.toString(), address!.latitude, address!.longitude, temp.getString("id")!);
                  if(response.statusCode==200){
                    Navigator.of(context).pop();
                  }
                }
              },),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        Row(
          children: [
            ButtonFormWidget(minWidth: width*0.6, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Cancel", textColor: Colors.white, textSize: 15,onPressed: (){Navigator.of(context).pop();},),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ],
    );
  }


}
