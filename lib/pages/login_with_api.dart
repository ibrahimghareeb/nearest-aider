import "package:flutter/material.dart";
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nearest_aider/models/login_model.dart';
import 'package:nearest_aider/pages/home.dart';
import 'package:nearest_aider/pages/map_page.dart';
import 'package:nearest_aider/utilis/connection_manger.dart';
import 'package:nearest_aider/widget/text_input.dart';

import '../utilis/response.dart';

class LoginWithAPI extends StatelessWidget {
  LoginWithAPI({Key? key}) : super(key: key);

  TextEditingController controllerEmail=TextEditingController();
  TextEditingController controllerPassword=TextEditingController();
  TextEditingController controllerPhone=TextEditingController();

  final formKey=GlobalKey<FormState>();

  String? validator(String val){
    if(val.isEmpty)
      return "please enter your username and password";
    return null;

  }


  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: width,
          height: height,
          child: Form(
            key: formKey,
            child: Column(
              children: [
                SizedBox(height: height*0.1,),
                Text("Login",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                Text("Welcome to login page"),
                SizedBox(height: height*0.1,),
                TextInputAPP(controller: controllerEmail,hintText: "Enter username",validate: (val){return validator(controllerEmail.text.toString());}),
                SizedBox(height: height*0.05,),
                TextInputAPP(controller: controllerPassword,hintText: "Enter password",validate: (val){return validator(controllerPassword.text.toString());},),
                SizedBox(height: height*0.05,),
                TextInputAPP(controller: controllerPhone,hintText: "Enter phone",validate: (val){return validator(controllerPassword.text.toString());},),
                SizedBox(height: height*0.05,),
                MaterialButton(
                  minWidth: width*0.8,
                  onPressed: () async{
                    // if(formKey.currentState!.validate()) {
                    //   Response response=await signup(controllerEmail.text.toString(), controllerPassword.text.toString(), controllerPhone.text.toString(), 0, 0, "injured");
                    //   if(response.statusCode==200){
                    //     LoginModel model=response.body;
                    //     if(model.success==1){
                    //       Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> HomePage(controllerEmail.text.toString())));
                    //     }else{
                    //       ScaffoldMessenger.of(context).showSnackBar(
                    //         SnackBar(content: Text(model.message!)));
                    //     }
                    //
                    //     }else{
                    //       ScaffoldMessenger.of(context).showSnackBar(
                    //         SnackBar(content: Text("No internet")));
                    //     }
                    // }

                    var result=await Navigator.of(context).push(MaterialPageRoute(builder: (builder)=>MapPage()));
                    if(result!=null){
                      LatLng t=result;
                      print(t.longitude);
                      print(t.latitude);
                    }
                  },
                  child: Text("Login"),
                  color: Colors.orangeAccent,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
//
//   Response response=await login(controllerEmail.text.toString(), controllerPassword.text.toString());
//   if(response.statusCode==200){
//   LoginModel model=response.body;
//   if(model.success==1){
//   Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> HomePage(controllerEmail.text.toString())));
//   }else{
//   ScaffoldMessenger.of(context).showSnackBar(
//   SnackBar(content: Text(model.message!)));
//   }
//
//   }else{
//   ScaffoldMessenger.of(context).showSnackBar(
//   SnackBar(content: Text("No internet")));
//   }
// }