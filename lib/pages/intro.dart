import 'package:flutter/material.dart';
import 'package:nearest_aider/pages/login.dart';
import 'package:nearest_aider/provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:steps_indicator/steps_indicator.dart';

class Intro extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/Intro.png")
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                SizedBox(height: height*0.2,),
                Text(Provider.of<AppProvider>(context).introText,style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 24),),

              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: width*0.05,right:width*0.05 ,bottom: height*0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    child: Text("skip",style:TextStyle(fontFamily: "Lato",fontSize: 18)),
                    onTap: (){
                      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Login()),ModalRoute.withName("/login"));
                    },
                  ),
                  StepsIndicator(
                    selectedStepColorIn: Color.fromRGBO(167, 22, 10, 1),
                    doneLineColor: Colors.white,
                    undoneLineColor: Colors.white,
                    undoneLineThickness: 0,
                    doneLineThickness: 0,
                    enableStepAnimation: true,
                    selectedStepSize: 10,
                    doneStepSize: 10,
                    unselectedStepSize: 10,
                    lineLength: 10,
                    selectedStepColorOut: Color.fromRGBO(167, 22, 10, 1),
                    unselectedStepColorOut: Colors.white,
                    unselectedStepColorIn: Color.fromRGBO(167, 22, 10, 0.2),
                    doneStepColor: Color.fromRGBO(167, 22, 10, 0.2),
                    selectedStep: Provider.of<AppProvider>(context).introCounter,
                    nbSteps: 3,
                  ),
                  InkWell(
                    child: Text("Next",style:TextStyle(fontFamily: "Lato",fontSize: 18)),
                    onTap: (){
                      Provider.of<AppProvider>(context,listen: false).changeIntroText();
                      if(Provider.of<AppProvider>(context,listen: false).introCounter==0) {
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Login()),ModalRoute.withName("/login"));
                      }
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
