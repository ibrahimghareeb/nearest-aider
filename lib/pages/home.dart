import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:nearest_aider/pages/edit_account.dart';
import 'package:nearest_aider/pages/get_location_map.dart';
import 'package:nearest_aider/pages/login.dart';
import 'package:nearest_aider/pages/my_contact.dart';
import 'package:nearest_aider/utilis/connection_manger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatelessWidget {

  String? username;


  HomePage(this.username);

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 251, 250, 1),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          Center(
            child: InkWell(
              onTap: () async{
                SharedPreferences temp=await SharedPreferences.getInstance();
                temp.remove("id");
                temp.remove("username");
                temp.remove("password");
                temp.remove("accountType");
                temp.setBool("login", false);
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> Login()),ModalRoute.withName("/login"));
              },
              child: Container(child: Text("Logout  ",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(248, 19, 0, 1)))),
            ),
          )
        ],
        title: Text("Hello $username!",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 24,color: Color.fromRGBO(248, 19, 0, 1))),
      ),
      body: Container(
        width: width,
        height: height,
        child: Column(
          children: [
            SizedBox(height: height*0.05,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> MyContact()));
                  },
                  child: Card(
                    elevation: 5,
                    shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide.none
                    ),
                    child: Container(
                      width: width*0.45,
                      height: height*0.15,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("My contact",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 16,color: Color.fromRGBO(167, 22, 10, 1))),
                          SizedBox(height: height*0.02,),
                          Image.asset("assets/images/ios-contacts.png")
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: width*0.02,),
                InkWell(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> EditMyAccount()));
                  },
                  child: Card(
                    elevation: 5,
                    shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide.none
                    ),
                    child: Container(
                      width: width*0.45,
                      height: height*0.15,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("Edit account",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 16,color: Color.fromRGBO(167, 22, 10, 1))),
                          SizedBox(height: height*0.02,),
                          Image.asset("assets/images/user.png")
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: height*0.03,),
            SizedBox(height: height*0.05,),
            InkWell(
              onTap: () async{
                SharedPreferences sharedPreference=await SharedPreferences.getInstance();
                addEmergencyRequest(sharedPreference.getString("id")!);
                await FlutterPhoneDirectCaller.callNumber.call("911");
              },
              splashColor: Color.fromRGBO(255, 227, 220, 1),
              child: Stack(
                children: [
                  Container(
                    width: width*0.38,
                    height: height*0.2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(70)),
                      color: Color.fromRGBO(255, 227, 220, 1)
                    ),
                  ),
                  Positioned(
                    top: height*0.045,
                    left: width*0.09,
                    child: Card(
                      shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(70)),
                        borderSide: BorderSide.none
                      ),
                      elevation: 10,
                      child: Container(
                        width: width*0.18,
                        height: height*0.1,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(70)),
                            color: Color.fromRGBO(198, 37, 23, 1)
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: height*0.02,),
            Text("Press the emergency button ",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 20,color: Color.fromRGBO(28, 28, 28, 1)),),
            SizedBox(height: height*0.02,),
          ],
        ),
      ),
    );
  }
}
