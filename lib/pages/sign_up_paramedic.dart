import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:nearest_aider/models/login_model.dart';
import 'package:nearest_aider/pages/get_location_map.dart';
import 'package:nearest_aider/pages/home_paramedic.dart';
import 'package:nearest_aider/pages/login.dart';
import 'package:nearest_aider/provider.dart';
import 'package:nearest_aider/utilis/response.dart';
import 'package:nearest_aider/widget/button.dart';
import 'package:nearest_aider/widget/text_field.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utilis/connection_manger.dart';

class SignUpParamedic extends StatefulWidget {
  SignUpParamedic({Key? key}) : super(key: key);

  @override
  State<SignUpParamedic> createState() => _SignUpParamedicState();
}

class _SignUpParamedicState extends State<SignUpParamedic> {
  final TextEditingController userName=TextEditingController();

  final TextEditingController password=TextEditingController();

  final TextEditingController phone=TextEditingController();

  LatLng? address;

  final formKey=GlobalKey<FormState>();

  File? certificateImage;

  String? validateEmail(String email){
    if (email.isEmpty) {
      return "please enter username";
    } else {
      return null;
    }
  }

  String? validatePassword(String password){
    if (password.isEmpty) {
      return "please enter password";
    } else if (password.length<8) {
      return "password length must be 8 character or more";
    } else {
      return null;
    }
  }

  Future<XFile?> getImageFromPhone() async{
    XFile? image=await ImagePicker().pickImage(source: ImageSource.gallery);
    if(image!.path.endsWith(".png")||
        image.path.endsWith(".jpg")||
        image.path.endsWith(".jpeg")) {
      return image;
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/sign_in_background.png")
              )
          ),
          width: width,
          height: height,
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("assets/images/logo.png"),
                SizedBox(height: height*0.05,),
                Text("Welcome",style: GoogleFonts.lato(fontWeight: FontWeight.bold,fontSize: 24),),
                Text("Sign Up to continue",style: GoogleFonts.lato(fontSize: 14),),
                SizedBox(height: height*0.05,),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInput(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 5, controller: userName, textInputType: TextInputType.emailAddress, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "User Name", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red, validate:(value) {return validateEmail(value!);}),
                ),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInput(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 15, controller: phone, textInputType: TextInputType.phone, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "Mobile Number", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red, validate:(value) {return validateEmail(value!);}),
                ),
                InkWell(
                    onTap: () async {var result =await Navigator.of(context).push(MaterialPageRoute(builder: (builder)=>GetLocationMap())); address=result; Provider.of<AppProvider>(context,listen: false).changeAddressLabel(address!.latitude.toString()+" "+address!.longitude.toString()); },
                    child: Padding(
                      padding: EdgeInsets.only(left: width*0.067,right: width*0.067),
                      child: Container(
                        //margin: EdgeInsets.only(top: height*0.02,bottom: height*0.02),
                        padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                        height: height*0.071,
                        width: width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(width: 1,color: Colors.grey)
                        ),
                        child: Row(

                          children: [
                            Text(Provider.of<AppProvider>(context).addressLabel,style: TextStyle(color: Colors.grey,fontSize: 12,),),
                          ],
                        ),
                      ),
                    )
                ),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInputPassword(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 15, controller: password, textInputType: TextInputType.visiblePassword, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "Password", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red,suffixIcon: const Icon(Icons.lock), validate:(value) {return validatePassword(value!);},resgister: false,),
                ),
                InkWell(
                    onTap: () async {XFile? temp=await getImageFromPhone(); if(temp!=null)setState(() {certificateImage=File(temp!.path);});  },
                    child: Padding(
                      padding: EdgeInsets.only(left: width*0.067,right: width*0.067),
                      child: Container(
                        //margin: EdgeInsets.only(top: height*0.02,bottom: height*0.02),
                        padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                        height: height*0.071,
                        width: width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(width: 1,color: Colors.grey)
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(certificateImage==null?"Certificate":"selected",style: TextStyle(color: Colors.grey,fontSize: 12,),),
                            Icon(Icons.file_upload_outlined,color: Colors.grey,)
                          ],
                        ),
                      ),
                    )
                ),
                Container(
                  margin: EdgeInsets.only(top: height*0.01),
                  child: ButtonFormWidget(
                    minWidth: width*0.87,
                    borderRadius: 10,
                    borderColor: Colors.white,
                    borderWidth: 0,
                    buttonText: "Sign Up",
                    textColor: Colors.white,
                    textSize: 15,
                    onPressed: () async{
                      if(formKey.currentState!.validate()){
                        if(address==null){
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("please select your location"),));
                          return;
                        }
                        if(certificateImage==null){
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("please select your certificate"),));
                          return;
                        }
                        Response response=await paramedicSignup(userName.text.toString(), password.text.toString(),phone.text.toString() , address!.latitude, address!.longitude, "paramedic",certificateImage!);
                        if(response.statusCode==200){
                          LoginModel log=response.body;
                          if(log.success==1){
                            SharedPreferences temp=await SharedPreferences.getInstance();
                            temp.setBool("login", true);
                            temp.setString("username", userName.text.toString());
                            temp.setString("password", password.text.toString());
                            temp.setString("accountType", "paramedic");
                            //temp.setString("id", log.id!);
                            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> Login()),ModalRoute.withName("/login"));
                            //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> HomePageParamedic(userName.text.toString())),ModalRoute.withName("/HomeParamedic"));
                          }
                        }
                      }
                    }
                  )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
