import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapPage extends StatefulWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {

  List<Marker> myMarker=[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            GoogleMap(
              initialCameraPosition: CameraPosition(
                  target: LatLng(0,0),
                  zoom: 5
              ),
              markers: Set.of(myMarker!),
              onTap: (val){
                // print(val.latitude);
                // print(val.longitude);
                myMarker=[];
                myMarker!.add(Marker(
                    markerId: MarkerId(val.toString()),
                    position: val,
                    icon: BitmapDescriptor.defaultMarker
                ));
                setState(() {

                });
              },
            ),
            Positioned(
              bottom: 0,
              child: Center(
                child: MaterialButton(
                  onPressed: (){
                    Navigator.of(context).pop(myMarker.first.position);
                  },
                  child: Text("ok"),
                  color: Colors.orange,
                ),
              ))
          ],
        ),
      )
    );
  }
}
