import 'package:flutter/material.dart';
import 'package:nearest_aider/models/login_model.dart';
import 'package:nearest_aider/pages/home.dart';
import 'package:nearest_aider/pages/home_paramedic.dart';
import 'package:nearest_aider/pages/sign_up.dart';
import 'package:nearest_aider/utilis/connection_manger.dart';
import 'package:nearest_aider/utilis/response.dart';
import 'package:nearest_aider/widget/button.dart';
import 'package:nearest_aider/widget/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatelessWidget {
  Login({Key? key}) : super(key: key);
  final TextEditingController userName=TextEditingController();
  final TextEditingController password=TextEditingController();
  final formKey=GlobalKey<FormState>();
  String? validateEmail(String email){
    if (email.isEmpty) {
      return "please enter username";
    } else {
      return null;
    }
  }
  String? validatePassword(String password){
    if (password.isEmpty) {
      return "please enter password";
    } else if (password.length<8) {
      return "password length must be 8 character or more";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/sign_in_background.png")
            )
          ),
          width: width,
          height: height,
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("assets/images/logo.png"),
                SizedBox(height: height*0.05,),
                Text("Welcome",style: TextStyle(fontFamily: "Lato",fontWeight: FontWeight.bold,fontSize: 24),),
                Text("Sign In to continue",style: TextStyle(fontFamily: "Lato",fontSize: 14),),
                SizedBox(height: height*0.05,),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInput(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 5, controller: userName, textInputType: TextInputType.emailAddress, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "User Name", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red,suffixIcon: const Icon(Icons.email), validate:(value) {return validateEmail(value!);}),
                ),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInputPassword(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 5, controller: password, textInputType: TextInputType.visiblePassword, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "Password", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red,suffixIcon: const Icon(Icons.lock), validate:(value) {return validatePassword(value!);},resgister: false,),
                ),
                Container(margin: EdgeInsets.only(top: height*0.01),child: ButtonFormWidget(minWidth: width*0.87, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Login", textColor: Colors.white, textSize: 15,
                  onPressed: ()async{
                    if(formKey.currentState!.validate()){
                      Response? response=await login(userName.text.toString(),password.text.toString());
                      if(response.statusCode==200){
                        LoginModel model=response.body;
                        if(model.success==1){
                          SharedPreferences temp=await SharedPreferences.getInstance();
                          temp.setBool("login", true);
                          temp.setString("username", userName.text.toString());
                          temp.setString("password", password.text.toString());
                          temp.setString("accountType", model.role!);
                          temp.setString("id", model.id!);
                          if(model.role=="paramedic")
                            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=>
                                HomePageParamedic(userName.text.toString())),ModalRoute.withName("/HomeParamedic"));
                          else
                            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=>
                                HomePage(userName.text.toString())),ModalRoute.withName("/Home"));
                        }
                        else{
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(model.message!,)));
                        }
                      }
                      else
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("connection failed")));

                    }
                  },
                )
                ),
                Container(margin: EdgeInsets.only(top: height*0.02),child: Row(mainAxisAlignment: MainAxisAlignment.center,children: [Text("if you don't have an account, "),InkWell(child: Container(child: Text("Sign up!",style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(167, 22, 10, 1)),)),onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> SignUp()));},)],))

              ],
            ),
          ),
        ),
      ),
    );
  }
}
