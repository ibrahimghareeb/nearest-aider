import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nearest_aider/models/login_model.dart';
import 'package:nearest_aider/pages/get_location_map.dart';
import 'package:nearest_aider/pages/home.dart';
import 'package:nearest_aider/pages/login.dart';
import 'package:nearest_aider/pages/sign_up_paramedic.dart';
import 'package:nearest_aider/provider.dart';
import 'package:nearest_aider/utilis/connection_manger.dart';
import 'package:nearest_aider/utilis/response.dart';
import 'package:nearest_aider/widget/button.dart';
import 'package:nearest_aider/widget/text_field.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUp extends StatelessWidget {
  SignUp({Key? key}) : super(key: key);

  final TextEditingController userName=TextEditingController();

  final TextEditingController password=TextEditingController();

  final TextEditingController phone=TextEditingController();

  LatLng? address;

  final formKey=GlobalKey<FormState>();

  String? validateEmail(String email){
    if (email.isEmpty) {
      return "please enter username";
    } else {
      return null;
    }
  }

  String? validatePassword(String password){
    if (password.isEmpty) {
      return "please enter password";
    } else if (password.length<8) {
      return "password length must be 8 character or more";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/sign_in_background.png")
              )
          ),
          width: width,
          height: height,
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("assets/images/logo.png"),
                SizedBox(height: height*0.05,),
                Text("Welcome",style: GoogleFonts.lato(fontWeight: FontWeight.bold,fontSize: 24),),
                Text("Sign Up to continue",style: GoogleFonts.lato(fontSize: 14),),
                SizedBox(height: height*0.05,),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInput(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 5, controller: userName, textInputType: TextInputType.emailAddress, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "User Name", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red, validate:(value) {return validateEmail(value!);}),
                ),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInput(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 15, controller: phone, textInputType: TextInputType.phone, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "Mobile Number", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red, validate:(value) {return validateEmail(value!);}),
                ),
                InkWell(
                    onTap: () async {var result =await Navigator.of(context).push(MaterialPageRoute(builder: (builder)=>GetLocationMap())); address=result; Provider.of<AppProvider>(context,listen: false).changeAddressLabel(address!.latitude.toString()+" "+address!.longitude.toString()); },
                    child: Padding(
                      padding: EdgeInsets.only(left: width*0.06,right: width*0.06),
                      child: Container(
                        //margin: EdgeInsets.only(top: height*0.02,bottom: height*0.02),
                        padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                        height: height*0.071,
                        width: width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(width: 0.5)
                        ),
                        child: Row(

                          children: [
                            Text(Provider.of<AppProvider>(context).addressLabel,style: TextStyle(color: Colors.grey,fontSize: 12,),),
                          ],
                        ),
                      ),
                    )
                ),
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                  child: TextInputPassword(leftMargin: 5, topMargin: 15, rightMargin: 5, bottomMargin: 5, controller: password, textInputType: TextInputType.visiblePassword, fontSizeAll: 12, colorFontStyle: Colors.black, hintText: "Password", hintSize: 12, hintColor: Colors.grey, labelSize: 20, labelColor: Colors.black, errorSize: 12, errorColor: Colors.red, borderRadius: 10, borderWidth: 0, borderColor: Colors.grey, enabledBorderRadius: 10, enabledBorderWidth: 1, enabledBorderColor: Colors.grey, focusedBorderRadius: 10, focusedBorderWidth: 1, focusedBorderColor: Colors.grey, errorBorderRadius: 10, errorBorderWidth: 1, errorBorderColor: Colors.red,suffixIcon: const Icon(Icons.lock), validate:(value) {return validatePassword(value!);},resgister: false,),
                ),
                Container(margin: EdgeInsets.only(top: height*0.01),child: ButtonFormWidget(minWidth: width*0.87, borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Sign Up", textColor: Colors.white, textSize: 15,
                  onPressed: () async{
                    if(formKey.currentState!.validate()&&address!=null){
                      Response response=await signup(userName.text.toString(), password.text.toString(),phone.text.toString() , address!.latitude, address!.longitude, "injured");
                      if(response.statusCode==200){
                        LoginModel log=response.body;
                        if(log.success==1){
                          SharedPreferences temp=await SharedPreferences.getInstance();
                          temp.setBool("login", true);
                          temp.setString("username", userName.text.toString());
                          temp.setString("password", password.text.toString());
                          temp.setString("accountType", "injured");
                          temp.setString("id", log.id!);
                          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> Login()),ModalRoute.withName("/login"));
                          //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> HomePage(userName.text.toString())),ModalRoute.withName("/Home"));
                        }else{
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(log.message!,)));
                        }
                      }
                    }
                    },
                )
                ),
                Container(margin: EdgeInsets.only(top: height*0.02),child: Row(mainAxisAlignment: MainAxisAlignment.center,children: [Text("or sin up as "),InkWell(child: Container(child: Text("paramedic!",style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(167, 22, 10, 1)),)),onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> SignUpParamedic()));},)],))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
