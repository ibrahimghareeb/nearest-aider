class NotificationModel{
  String? requestId;
  String? latitude;
  String? longitude;

  NotificationModel.fromJson(Map<String,dynamic> json){
    requestId=json["request_id"];
    latitude=json["latitude"];
    longitude=json["longitude"];
  }
}