class ContactModel{
  String? name;
  String? latitude;
  String? longitude;
  String? contactId;


  ContactModel.fromJson(Map<String,dynamic> json){
    name=json["name"];
    latitude=json["latitude"];
    longitude=json["longitude"];
    contactId=json["id"];
  }
}